<?php

namespace tests\codeception\api\unit\services;

use api\modules\api\v1\models\Cart;
use PHPUnit\Framework\TestCase;
use api\modules\api\v1\models\query\CartQuery;
use api\modules\api\v1\services\CartService;
use yii\web\Request;

class CartServiceTest extends TestCase
{
    /** @var CartService */
    protected $service;

    public function setUp()
    {
        $this->service = new CartService();
    }

    public function testFindCart()
    {
        /* test data */
        $id = 0;

        $model = $this->getCartModelMock();
        $model->id = $id;
        $repository = $this->getCartRepositoryMock();
        $repository
            ->expects($this->once())
            ->method('findById')
            ->with($id)
            ->willReturn($model);
        $result = $this->service->findCart($repository, $id);
        $this->assertEquals($result->id, $id);
    }

    public function testFindFailed()
    {
        $repository = $this->getCartRepositoryMock();
        $repository
            ->expects($this->once())
            ->method('findById')
            ->willReturn(null);
        $this->expectException(\InvalidArgumentException::class);

        $this->service->findCart($repository, 0);
    }

    public function testCreateCart()
    {
        /* test data */
        $dummyDiscount = $this->getDummyDiscount(10, 1);
        $dummyProduct1 = $this->getDummyProduct(30);
        $dummyProduct2 = $this->getDummyProduct(20);
        $expectedTotalPrice = 50;
        $expectedTotalDiscountedPrice = 40;


        $model = $this->getCartModelMock();
        $model
            ->expects($this->once())
            ->method('load');
        $model->id = 0;
        $model->products = [$dummyProduct1, $dummyProduct2];
        $model->discount = $dummyDiscount;
        $model
            ->expects($this->once())
            ->method('validate')
            ->willReturn(true);
        $model
            ->expects($this->atLeastOnce())
            ->method('save')
            ->willReturn(true);

        $repository = $this->getCartRepositoryMock();
        $repository
            ->expects($this->once())
            ->method('createCart')
            ->willReturn($model);
        $repository
            ->expects($this->once())
            ->method('addProducts');

        $request = $this->getRequestMock();

        $result = $this->service->createCart($repository, $request);
        $this->assertEquals($result->total, $expectedTotalPrice);
        $this->assertEquals($result->discounted_total, $expectedTotalDiscountedPrice);
    }

    public function testUpdateCart()
    {
        /* test data */
        $id = 0;
        $dummyDiscount = $this->getDummyDiscount(10, 1);
        $dummyProduct1 = $this->getDummyProduct(30);
        $dummyProduct2 = $this->getDummyProduct(20);
        $expectedTotalPrice = 50;
        $expectedTotalDiscountedPrice = 40;

        $model = $this->getCartModelMock();
        $model
            ->expects($this->once())
            ->method('load');
        $model->id = 0;
        $model->products = [$dummyProduct1, $dummyProduct2];
        $model->discount = $dummyDiscount;
        $model
            ->expects($this->once())
            ->method('validate')
            ->willReturn(true);
        $model
            ->expects($this->atLeastOnce())
            ->method('save')
            ->willReturn(true);

        $repository = $this->getCartRepositoryMock();
        $repository
            ->expects($this->once())
            ->method('findById')
            ->with($id)
            ->willReturn($model);
        $repository
            ->expects($this->once())
            ->method('addProducts');
        $repository
            ->expects($this->once())
            ->method('removeExcessProducts');

        $request = $this->getRequestMock();

        $result = $this->service->updateCart($repository, $request, $id);
        $this->assertEquals($result->total, $expectedTotalPrice);
        $this->assertEquals($result->discounted_total, $expectedTotalDiscountedPrice);
    }

    /**
     * @return Cart | \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getCartModelMock()
    {
        $model = $this->getMockBuilder(Cart::class)
            ->disableOriginalConstructor()
            ->setMethods(['attributes', 'save', 'load', 'validate'])
            ->getMock();
        $model
            ->method('attributes')
            ->willReturn(['id', 'total', 'discounted_total', 'discount', 'products']);

        return $model;
    }

    /**
     * @return CartQuery | \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getCartRepositoryMock()
    {
        return $this->getMockBuilder(CartQuery::class)
            ->disableOriginalConstructor()
            ->setMethods(['findById', 'createCart', 'removeExcessProducts', 'addProducts'])
            ->getMock();
    }

    /**
     * @return Request | \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getRequestMock()
    {
        return $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(['post'])
            ->getMock();
    }

    protected function getDummyDiscount($value, $type)
    {
        $dummyDiscount = new \stdClass();
        $dummyDiscount->type = $type;
        $dummyDiscount->value = $value;
        return $dummyDiscount;
    }

    protected function getDummyProduct($price)
    {
        $dummyDiscount = new \stdClass();
        $dummyDiscount->title = "title";
        $dummyDiscount->price = $price;
        return $dummyDiscount;
    }
}
