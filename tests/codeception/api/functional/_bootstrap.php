<?php
require(__DIR__ . '/../_bootstrap.php');

// Environment
require(YII_APP_BASE_PATH . '/console/env.php');

$config = require(dirname(dirname(__DIR__)) . '/config/api/functional.php');

new yii\web\Application($config);
