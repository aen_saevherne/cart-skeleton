<?php
/**
 * Application configuration shared by all applications and test types
 */
return [
    'controllerMap' => [
        'fixture' => [
            'class' => yii\faker\FixtureController::class,
            'fixtureDataPath' => '@tests/common/fixtures/data',
            'templatePath' => '@tests/common/templates/fixtures',
            'namespace' => 'tests\common\fixtures',
        ],
    ],
    'components' => [
        'mongodb' => [
            'class' => yii\mongodb\Connection::class,
            'dsn' => 'mongodb://' . getenv('MONGODB_TEST_USER')
                . ':' . getenv('MONGODB_TEST_PASS')
                . '@' . getenv('MONGODB_TEST_HOST')
                . ':' . getenv('MONGODB_TEST_PORT')
                . '/' . getenv('MONGODB_TEST_DB'),
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
    'modules' => [
        'authfilter' => [
            'class' => indigerd\oauth2\authfilter\Module::class,
            'testMode' => true,
        ],
    ],
];
