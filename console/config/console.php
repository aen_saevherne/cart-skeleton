<?php

$config = [
    'name' => 'Yii2 Cart Console CLI',
    'id' => 'console',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@api/modules/api/migrations', //leave as it was before
            'migrationTable' => '{{%system_db_migration}}' //leave as it was before
        ],
    ],
    'components' => [
        'cache' => [
            'class' => yii\caching\DummyCache::class,
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8',
            'enableSchemaCache' => false,
        ],
    ],
];

return $config;
