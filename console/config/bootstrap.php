<?php
// Path aliases
Yii::setAlias('@base', realpath(__DIR__ . '/../../'));
Yii::setAlias('@console', realpath(__DIR__ . '/../../console'));
Yii::setAlias('@api', realpath(__DIR__ . '/../../api'));
Yii::setAlias('@tests', realpath(__DIR__ . '/../../tests'));
