#!/bin/bash
export COMPOSE_HTTP_TIMEOUT=600
cp -f docker-compose-local.yml docker-compose.yml
cp -f .env.dist .env
docker-compose build
docker-compose down -v
docker-compose up -d
docker-compose logs -f appsetup
echo "For project management use docker-compose command in current dir"
