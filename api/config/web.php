<?php
$config = [
    'homeUrl' => getenv('API_URL'),
    'modules' => [
        'api' => [
            'class' => api\modules\api\Module::class,
            'modules' => [
                'v1' => api\modules\api\v1\Module::class
            ]
        ],
    ],
];

return $config;
