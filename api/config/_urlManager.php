<?php
return [
    'class' => yii\web\UrlManager::class,
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Api
        [
            'class' => yii\rest\UrlRule::class,
            'controller' => 'api/v1/cart',
            'except' => ['delete'],
            'extraPatterns' => [
                'POST {id}' => 'update'
            ]
        ],
    ]
];
