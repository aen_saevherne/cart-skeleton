<?php

return  [
    'id' => 'api',
    'name' => 'Service Cart',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'urlManager' => require(__DIR__ . '/_urlManager.php'),
        'cache' => [
            'class' => yii\caching\DummyCache::class,
        ],
        'request' => [
            'enableCookieValidation' => false,
            'cookieValidationKey' => '',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8',
            'enableSchemaCache' => false,
        ],
    ],
    'container' => [
        'definitions' => [
            api\modules\api\v1\services\CartServiceInterface::class => [
                'class' => api\modules\api\v1\services\CartService::class
            ],
        ],
    ],
];
