<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cart_products`.
 */
class m180930_122354_create_cart_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cart_products', [
            'id' => $this->primaryKey(),
            'cart_id' => $this->integer(),
            'product_id' => $this->integer(),
        ]);

        $this->createIndex('idx-cart_id', 'cart_products', 'cart_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cart_products');
    }
}
