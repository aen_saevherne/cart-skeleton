<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discounts`.
 */
class m180930_121825_create_discounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('discounts', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger(),
            'value' => $this->float()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('discounts');
    }
}
