<?php

use yii\db\Migration;

/**
 * Handles the creation of table `carts`.
 */
class m180930_122029_create_carts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('carts', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'discount_id' => $this->integer(),
            'total' => $this->decimal(13, 2),
            'discounted_total' => $this->decimal(13, 2),
            'updated_at' => $this->bigInteger(),
            'created_at' => $this->bigInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('carts');
    }
}
