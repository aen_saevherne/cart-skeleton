<?php

use yii\db\Migration;

/**
 * Class m180930_122731_insert_fixtures_for_products_users_discounts
 */
class m180930_122731_insert_fixtures_for_products_users_discounts extends Migration
{
    const USERS_FIXTURE = 'api/modules/api/migrations/fixtures/users.json';
    const PRODUCTS_FIXTURE = 'api/modules/api/migrations/fixtures/products.json';
    const DISCOUNTS_FIXTURE = 'api/modules/api/migrations/fixtures/discounts.json';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = json_decode(file_get_contents(static::USERS_FIXTURE), true);
        $this->batchInsert('users', ['id', 'first_name', 'last_name'], $users);

        $products = json_decode(file_get_contents(static::PRODUCTS_FIXTURE), true);
        $this->batchInsert('products', ['id', 'title', 'price'], $products);

        $discounts = json_decode(file_get_contents(static::DISCOUNTS_FIXTURE), true);
        $this->batchInsert('discounts', ['id', 'type', 'value'], $discounts);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('users');

        $this->truncateTable('products');

        $this->truncateTable('discounts');
    }
}
