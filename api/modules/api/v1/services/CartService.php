<?php

namespace api\modules\api\v1\services;

use api\modules\api\v1\models\Cart;
use api\modules\api\v1\models\Discount;
use api\modules\api\v1\models\query\CartQuery;
use yii\web\Request;

class CartService implements CartServiceInterface
{
    /**
     * @inheritdoc
     */
    public function createCart(CartQuery $cartRepository, Request $request)
    {
        $params = $request->post();
        $productIDs = empty($params['Cart']['products']) ? [] : $params['Cart']['products'];

        $cart = $cartRepository->createCart();
        $cart->load($params);
        if ($cart->validate()) {
            $cart->save();
            $cartRepository->addProducts($cart, $productIDs);
        }

        $this->calculateCartPrice($cart);
        return $cart;
    }

    /**
     * @inheritdoc
     */
    public function updateCart(CartQuery $cartRepository, Request $request, $id)
    {

        $params = $request->post();
        $productIDs = empty($params['Cart']['products']) ? [] : $params['Cart']['products'];

        $cart = $this->findCart($cartRepository, $id);
        $cart->load($params);
        if ($cart->validate()) {
            $cartRepository->removeExcessProducts($cart, $productIDs);
            $cartRepository->addProducts($cart, $productIDs);
        }

        $this->calculateCartPrice($cart);
        return $cart;
    }

    /**
     * @inheritdoc
     */
    public function findCart(CartQuery $cartRepository, $id)
    {
        if ($cart = $cartRepository->findById($id)) {
            return $cart;
        }

        throw new \InvalidArgumentException('Invalid ID');
    }

    /**
     * Calculates and saves cart total and discounted_total fields
     *
     * @param Cart $cart
     */
    protected function calculateCartPrice(Cart $cart)
    {
        $total = 0;
        foreach ($cart->products as $product) {
            $total += $product->price;
        }
        $cart->total = $total;

        $discountedTotal = $total;
        if ($discount = $cart->discount) {
            $discountedTotal = ($discount->type == Discount::ABSOLUTE_DISCOUNT)
                ? $total - $discount->value
                : $total - ($total * $discount->value) / 100;
        }
        $cart->discounted_total = $discountedTotal < 0 ? 0 : $discountedTotal;
        $cart->save();
    }
}
