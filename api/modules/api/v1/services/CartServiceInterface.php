<?php

namespace api\modules\api\v1\services;

use api\modules\api\v1\models\Cart;
use api\modules\api\v1\models\query\CartQuery;
use yii\web\Request;

interface CartServiceInterface
{
    /**
     * Create new cart from request params
     *
     * @param CartQuery $cartRepository
     * @param Request $request
     * @return Cart
     */
    public function createCart(CartQuery $cartRepository, Request $request);

    /**
     * Update existing cart from request params
     *
     * @param CartQuery $cartRepository
     * @param Request $request
     * @param $id
     * @return Cart
     */
    public function updateCart(CartQuery $cartRepository, Request $request, $id);

    /**
     * Find cart by ID
     *
     * @param CartQuery $cartRepository
     * @param $id
     * @return Cart
     */
    public function findCart(CartQuery $cartRepository, $id);

}
