<?php

/**
 * @apiDefine AuthRequired
 *
 * @apiHeader {String} Authorization Access token. Use either the Authorization header
 * or the access_token query param to pass an access token.
 *
 * @apiParam {String} access_token Access token. Use either the Authorization header
 * or the access_token query param to pass an access token.
 */

/**
 * @apiDefine NotFoundError
 *
 * @apiError NotFound Item was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *         "name": "Not Found",
 *         "message": "Page not found: page-url",
 *         "code": 404,
 *         "status": 404
 *     }
 */

/**
 * @apiDefine BadRequestError
 *
 * @apiError BadRequestError Bad request.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 400 Bad Request.
 *     {
 *       "name":"Bad Request","message":"Wrong token","code":400,"status":400
 *     }
 */

/**
 * @apiDefine none No access rights needed
 * Everyone allowed to execute this method
 * @apiVersion 1.0.0
 */

/**
 * @apiDefine user User access rights needed
 * To execute this method needs OAuth token provided with grant type <code>password</code>
 * @apiVersion 1.0.0
 */

/**
 * @apiDefine application Application access rights needed
 * To execute this method needs OAuth token provided with grant type <code>client_credentials</code>
 * @apiVersion 1.0.0
 */
