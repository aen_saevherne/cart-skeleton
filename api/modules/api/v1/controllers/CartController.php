<?php
namespace api\modules\api\v1\controllers;

use api\modules\api\v1\models\Cart;
use api\modules\api\v1\services\CartServiceInterface;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CartController extends Controller
{
    public $layout = 'main';

    /** @var CartServiceInterface */
    protected $cartService;

    public function __construct($id, $module, CartServiceInterface $cartService, array $config = [])
    {
        $this->cartService = $cartService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Index action
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('@app/modules/api/v1/views/index', [
            'model' => new Cart()
        ]);
    }

    /**
     * Update cart action
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionUpdate($id) {
        $cart = $this->cartService->updateCart(Cart::find(), \Yii::$app->getRequest(), $id);

        return $this->redirect(['view', 'id' => $cart->id]);
    }

    /**
     * Create cart action
     *
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {
        try {
            $cart = $this->cartService->createCart(Cart::find(), \Yii::$app->getRequest());

            return $this->redirect(['view', 'id' => $cart->id]);
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }


    /**
     * View cart action
     *
     * @param $id
     * @return string
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        try {
            $cart = $this->cartService->findCart(Cart::find(), $id);

            return $this->render('@app/modules/api/v1/views/index', [
                'model' => $cart,
                'id' => $id
            ]);
        } catch (\InvalidArgumentException $e) {
            throw new NotFoundHttpException('Cart not found');
        } catch (\Exception $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }
}
