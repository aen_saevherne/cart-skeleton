<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use api\modules\api\v1\models\Discount;
use api\modules\api\v1\models\User;
use api\modules\api\v1\models\Product;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \api\modules\api\v1\models\Cart */
/* @var $id integer|null */

$this->title = empty($id) ? 'Create Cart' : 'Update Cart' ;

$allDiscounts = Discount::find()->all();
$formattedDiscounts = [];
foreach ($allDiscounts as $discount) {
    $formattedDiscounts[$discount->id] = $discount->value . (($discount->type == Discount::ABSOLUTE_DISCOUNT) ? '$' : '%');
}

$allUsers = User::find()->all();
$users = ArrayHelper::map($allUsers, 'id', 'first_name');

$allProducts = Product::find()->all();
$products = ArrayHelper::map($allProducts, 'id', 'title');

?>
<p>
    <?php
    if (!empty($id)) {
        echo Html::a('Create new cart', ['index'], ['class' => 'btn btn-primary']);
    }
    ?>
</p>
<h1>
    <?= Html::encode($this->title) ?>
</h1>

<?php $form = ActiveForm::begin(); ?>
<?= $form
    ->field($model, 'user_id')
    ->textInput()
    ->dropDownList($users, [
        'options' => [
            $model->user_id => ['selected' => true]
        ]
    ])
    ->label('User:')
?>


<?= $form
    ->field($model, 'discount_id')
    ->dropDownList($formattedDiscounts, [
        'options' => [
            $model->discount_id => ['selected' => true]
        ]
    ])
    ->label('Discount:'); ?>

<?= $form
    ->field($model, 'products')
    ->dropDownList($products, [
        'multiple'=>'multiple',
    ])
    ->label("Add Products:");
?>

<?php
if (!empty($model->total)) {
    echo $form
        ->field($model, 'total')
        ->textInput()
        ->staticControl()
        ->label('Total:');
}
?>

<?php
if (!empty($model->total)) {
    echo $form
        ->field($model, 'discounted_total')
        ->textInput()
        ->staticControl()
        ->label('Discounted Total:');
}
?>
<?= Html::submitButton(empty($id) ? 'Create' : 'Update') ?>
<?php ActiveForm::end(); ?>

