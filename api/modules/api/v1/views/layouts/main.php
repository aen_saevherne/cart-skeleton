<?php

use yii\helpers\Html;

/* @var $content */

?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?php echo Yii::$app->language ?>">
    <head>
        <meta charset="<?php echo Yii::$app->charset ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <?php echo Html::csrfMetaTags() ?>
        <title><?php echo Yii::$app->name ?>::<?php echo Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>
    <?php $this->beginBody() ?>
    <?php echo $content ?>

    <?php $this->endBody() ?>
    <?php echo Html::endTag('body') ?>
    </html>
<?php $this->endPage() ?>