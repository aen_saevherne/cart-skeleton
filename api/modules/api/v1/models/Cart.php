<?php

namespace api\modules\api\v1\models;

use api\modules\api\v1\models\query\CartQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Cart Model
 *
 * @property int $id
 * @property int $user_id
 * @property int $discount_id
 * @property float $total
 * @property float $discounted_total
 * @property int $updated_at
 * @property int $created_at
 * @property Product[] $products
 * @property Discount $discount
 */
class Cart extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'discount_id'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%carts}}';
    }

    /**
     * @return CartQuery|object
     */
    public static function find()
    {
        return \Yii::createObject(CartQuery::class, [get_called_class()]);
    }

    public function getDiscount()
    {
        return $this->hasOne(Discount::class, ['id' => 'discount_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getCartProducts()
    {
        return $this->hasMany(CartProduct::class, ['cart_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])
            ->via('cartProducts');
    }
}
