<?php

namespace api\modules\api\v1\models;

use yii\db\ActiveRecord;

/**
 * User Model
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 */
class User extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%users}}';
    }
}
