<?php

namespace api\modules\api\v1\models\query;

use api\modules\api\v1\models\Cart;
use api\modules\api\v1\models\CartProduct;
use api\modules\api\v1\models\Product;
use yii\db\ActiveQuery;

class CartQuery extends ActiveQuery
{
    /**
     * @return Cart
     */
    public function createCart()
    {
        return new $this->modelClass;
    }

    /**
     * @param $id
     * @return array|null|Cart
     */
    public function findById($id)
    {
        return $this->where(['id' => $id])->one();
    }

    /**
     * Find cart product by cart ID and product ID
     *
     * @param $cartId
     * @param $productId
     * @return array|null|CartProduct
     */
    public function findCartProductById($cartId, $productId)
    {
        return CartProduct::find()
            ->where(['cart_id' => $cartId])
            ->andWhere(['product_id' => $productId])
            ->one();
    }

    /**
     * Remove cart products relation if they are not in the product ID list
     *
     * @param Cart $cart
     * @param $productIDs
     */
    public function removeExcessProducts(Cart $cart, $productIDs)
    {
        foreach ($cart->products as $product) {
            if (!in_array($product->id, $productIDs)) {
                $cart->unlink('products', $product);
            }
        }
    }

    /**
     * Add products to cart by product ID list
     *
     * @param Cart $cart
     * @param $productIDs
     */
    public function addProducts(Cart $cart, $productIDs)
    {
        foreach ($productIDs as $id) {
            //skip if relation already exist
            if ($this->findCartProductById($cart->id, $id)) {
                continue;
            };

            $product = Product::findOne($id);
            $cart->link('products', $product);
        }
    }
}
