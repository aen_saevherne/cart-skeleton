<?php

namespace api\modules\api\v1\models;

use yii\db\ActiveRecord;

/**
 * Product Model
 *
 * @property int $id
 * @property string $title
 * @property float $price
 */
class Product extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%products}}';
    }
}
