<?php

namespace api\modules\api\v1\models;

use yii\db\ActiveRecord;

/**
 * Discount Model
 *
 * @property int $id
 * @property int $type
 * @property int $value
 */
class Discount extends ActiveRecord
{
    const ABSOLUTE_DISCOUNT = 1;
    const PERCENT_DISCOUNT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%discounts}}';
    }
}
