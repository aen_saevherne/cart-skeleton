<?php

namespace api\modules\api\v1\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Cart Product Model
 *
 * @property int $id
 * @property int $cart_id
 * @property int $product_id
 */
class CartProduct extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cart_products}}';
    }
}
