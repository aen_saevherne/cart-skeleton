<?php

namespace api\modules\api;

use yii\base\Module as BaseModule;

/**
 * Class Module
 * @package api\modules\api
 * @property string $migrationPath
 */
class Module extends BaseModule
{
    public function getMigrationPath()
    {
        return __DIR__ . '/migrations';
    }

    public function init()
    {
        parent::init();
    }
}
