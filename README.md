# General Info
To test cart service functionality you need to open url:
http://localhost:8898/api/v1/carts

When cart created you can see it by url: http://localhost:8898/api/v1/carts/<cart_id>

# Start application:
Run command
./start.sh


# Unit tests:
Run command
./unit.sh





